/*
 * Copyright 2018 Blue Lotus Software, LLC..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluelotussoftware.utils;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;

/**
 * A simple utility to copy files.
 *
 * @author John Yeary <jyeary@bluelotussoftware.com>
 * @version 1.0
 */
public class FileCopy {

    private static boolean deleteTarget = false;

    public static void main(String[] args) throws IOException {

        if (args.length < 2) {
            System.out.println("Usage: com.bluelotussoftware.utils.FileCopy <source> <target>");
            System.out.println("Usage: com.bluelotussoftware.utils.FileCopy <source> <target> <deleteTargetDirectory:true>");
            System.exit(1);
        }

        File source = new File(args[0]);
        File target = new File(args[1]);
        if (args.length == 3 && !args[2].isEmpty()) {
            deleteTarget = Boolean.parseBoolean(args[2]);
        }

        if (deleteTarget) {
            FileUtils.deleteDirectory(target);
        }
        FileUtils.copyDirectoryToDirectory(source, target);
    }
}
